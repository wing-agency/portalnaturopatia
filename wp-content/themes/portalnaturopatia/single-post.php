<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/post-header'); ?>

	<main id="content" class="<?php // echo odin_classes_page_sidebar(); ?>" tabindex="-1" role="main">
		<?php
			// Start the Loop.
			

				/*
				 * Include the post format-specific template for the content. If you want to
				 * use this in a child theme, then include a file called called content-___.php
				 * (where ___ is the post format) and that will be used instead.
				 */
				get_template_part( 'content', 'post' );

				// If comments are open or we have at least one comment, load up the comment template.
				// if ( comments_open() || get_comments_number() ) :
				// 	comments_template();
				// endif;
			
		?>
	</main><!-- #main -->

	<?php get_template_part( 'template-parts/related-posts'); ?>

<?php endwhile; ?>

<?php
// get_sidebar();
get_footer();
