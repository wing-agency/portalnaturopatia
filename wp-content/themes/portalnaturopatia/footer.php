<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main div element.
 *
 * @package Odin
 * @since 2.2.0
 */
?>

		<!-- </div> --><!-- .row -->
	</div><!-- #wrapper -->

	<?php get_template_part('template-parts/newsletter'); ?>

	<?php $general = get_option('odin_general'); ?>
	<?php $social = get_option('odin_social'); ?>
	<footer id="footer" role="contentinfo">
		<h2 class="hidden">Rodapé</h2>
		<div class="container">
			<div class="row">
				<div class="company widget col-xs-12 col-sm-4 col-md-3">
					
					<div class="footer-logo">
						<?php odin_the_custom_logo(); ?>
					</div>

					


				</div>
				<div class="social-menu widget col-xs-12 col-sm-4 col-md-3">
					<?php 
					$facebook  = $social['facebook']; 
					$instagram  = $social['instagram']; 
					$linkedin  = $social['linkedin']; 
					?>
					<ul class="social">
						<?php if($facebook): ?>
						<li>
							<a href="<?php echo $facebook; ?>" class="facebook" target="_blank" title="Facebook">
								<i class="fa fa-facebook-square"></i>
								<span>Facebook</span>
							</a>
						</li>
						<?php endif; ?>

						<?php if($instagram): ?>
						<li>
							<a href="<?php echo $instagram; ?>" class="instagram" target="_blank" title="Instagram">
								<i class="fa fa-instagram"></i>
								<span>Instagram</span>
							</a>
						</li>
						<?php endif; ?>

						<?php if($linkedin): ?>
						<li>
							<a href="<?php echo $linkedin; ?>" class="linkedin" target="_blank" title="LinkedIn">
								<i class="fa fa-linkedin-square"></i>
								<span>LinkedIn</span>
							</a>
						</li>
						<?php endif; ?>
					</ul>
				</div>

				<?php dynamic_sidebar('footer-sidebar'); ?>

				<div class="partner widget col-xs-12 col-sm-4 col-md-3">
					<h3 class="widget-title">Parceiro</h3>
					<a href="https://www.prosaudeloja.com.br/" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-prosaude.png">
					</a>
				</div>

			</div>
			<div class="footer-bottom">
				<div class="copy">
					<p><a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a> &copy; <?php echo date( 'Y' ); ?>. <span><?php _e( 'All rights reserved', 'odin' ); ?></span></p>
					
				</div>
				<div class="wing">
					Desenvolvido por 
					<a href="http://www.agenciawing.com.br" target="_blank" title="Agência Wing">
						Agência Wing
					</a>
				</div>
			</div>
		</div><!-- .container -->
	</footer><!-- #footer -->

	<?php wp_footer(); ?>
</body>
</html>
