<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

<section class="institution-header" >
	<div class="container">
		<div class="row">
			<header class="entry-header col-xs-12">
				<?php 
				$logo = get_post_meta(get_the_ID(), 'logo', true)['guid'];
				$sigla = get_post_meta(get_the_ID(), 'sigla', true); 
				?>
				<div class="logo" style="background-image: url('<?php echo $logo; ?>');">
					<img class="hidden" src="<?php echo $logo; ?>">
				</div>
				<h1 class="entry-title">
					<?php echo $sigla; ?> - <?php the_title(); ?>
				</h1>
				
			</header>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<header class="professionals-list-header col-xs-12">
			<h2 class="title">Profissionais dessa instituição</h2>
		</header>
		<main id="content" class="professionals <?php // echo odin_classes_page_sidebar(); ?>" tabindex="-1" role="main">
			<?php 
			$authors = get_users( array(
			    'orderby'       => 'name',
			    'order'         => 'ASC',
			    'number'        => -1
			) );
			?>
			<?php foreach ($authors as $author): ?>
				<?php 
				$occupation = get_the_author_meta('occupation', $author->ID);
				$author_name = get_the_author_meta('first_name', $author->ID).' '.get_the_author_meta('last_name', $author->ID);
				$avatar = get_avatar_url($author->ID);
				$link = get_author_posts_url($author->ID);
				?>
				<div class="professional col-xs-6 col-sm-3 col-md-2">
					<a class="box" href="<?php echo $link; ?>">
						<div class="avatar" style="background-image: url('<?php echo $avatar; ?>');">
							<img class="hidden" src="<?php echo $avatar; ?>">
						</div>
						
						<h3 class="name"><?php echo $author_name; ?></h3>
						<span class="occupation"><?php echo $occupation; ?></span>
					</a>
				</div>
			<?php endforeach; ?>
		</main><!-- #content -->
	</div>
</div>

<?php
// get_sidebar();
get_footer();
