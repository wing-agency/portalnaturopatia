<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till #main div
 *
 * @package Odin
 * @since 2.2.0
 */
?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<script src="https://kit.fontawesome.com/7f76f5f5b6.js" crossorigin="anonymous"></script>
	<?php if ( ! get_option( 'site_icon' ) ) : ?>
		<link href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" rel="shortcut icon" />
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<a id="skippy" class="sr-only sr-only-focusable" href="#content">
		<div class="container">
			<span class="skiplink-text"><?php _e( 'Skip to content', 'odin' ); ?></span>
		</div>
	</a>

	<header id="header" role="banner">
		<div class="container">
			<div class="row">
				<div class="logo-header col-xs-9 col-sm-5 col-md-3 ">

					<?php if ( is_front_page() ) : ?>

						<?php odin_the_custom_logo(); ?>

						<div class="site-title hidden">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<?php bloginfo( 'name' ); ?>
							</a>
						</div>
						<div class="site-description hidden"><?php bloginfo( 'description' ); ?></div>

					<?php else: ?>

						<?php odin_the_custom_logo(); ?>

					<?php endif; ?>

				</div><!-- .page-header-->

				<div id="main-navigation" class="navbar navbar-default col-md-6">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed">
							<span class="sr-only"><?php _e( 'Toggle navigation', 'odin' ); ?></span>
							<!-- <span class="menu-label">menu</span> -->
							<span class="icon-bar top-bar"></span>
							<span class="icon-bar middle-bar"></span>
							<span class="icon-bar bottom-bar"></span>
						</button>
					</div>
					<nav class="collapse navbar-collapse navbar-main-navigation" role="navigation">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'main-menu',
									'depth'          => 2,
									'container'      => false,
									'menu_class'     => 'nav navbar-nav',
									'fallback_cb'    => 'Odin_Bootstrap_Nav_Walker::fallback',
									'walker'         => new Odin_Bootstrap_Nav_Walker()
								)
							);
						?>
					</nav><!-- .navbar-collapse -->
					
				</div><!-- #main-navigation-->
				<?php get_template_part( 'template-parts/header-cta'); ?>
			</div>

		</div><!-- .container-->
	</header><!-- #header -->

	<div id="wrapper" class="<?php if(is_page('minha-conta')) echo 'account-page'; ?>">
		<!-- <div class="row"> -->