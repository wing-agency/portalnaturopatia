<?php
/**
 * The default template for displaying content.
 *
 * Used for both single and index/archive/author/catagory/search/tag.
 *
 * @package Odin
 * @since 2.2.0
 */
?>
<?php 
$title = get_the_title();
$logo = get_post_meta(get_the_ID(), 'logo', true)['guid'];
$link = get_the_permalink(get_the_ID());
?>

<?php if(!is_single()): ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class('institution col-xs-6 col-sm-3 col-md-2'); ?>>
		<a class="box" href="<?php echo $link; ?>">
			<div class="logo" style="background-image: url('<?php echo $logo; ?>');">
				<img class="hidden" src="<?php echo $logo; ?>">
			</div>
			<h3 class="hidden"><?php echo $title; ?></h3>
		</a>
	</article><!-- #post-## -->

<?php else: ?>

<?php endif; ?>