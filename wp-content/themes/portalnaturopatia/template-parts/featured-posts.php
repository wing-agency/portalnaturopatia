<?php 
$args = array(
	'post_type' => 'post',
	'posts_per_page' => 4
);
$posts = get_posts($args); 

if($posts):
?>

	<?php

	function adjustBrightness($hex, $steps) {
		// Steps should be between -255 and 255. Negative = darker, positive = lighter
		$steps = max(-255, min(255, $steps));

		// Normalize into a six character long hex string
		$hex = str_replace('#', '', $hex);
		if (strlen($hex) == 3) {
			$hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
		}

		// Split into three parts: R, G and B
		$color_parts = str_split($hex, 2);
		$return = '#';

		foreach ($color_parts as $color) {
			$color   = hexdec($color); // Convert to decimal
			$color   = max(0,min(255,$color + $steps)); // Adjust color
			$return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
		}

		return $return;
	}
	?>

	<section id="home-blog" class="home-blog blog">
		<div class="container">
			<div class="row" >
				<header class="header col-xs-12 ">
					<h2 class="title">Postagens mais recentes</h2>

					<a class="" href="<?php echo esc_url( home_url('/blog') ); ?>">
						Ver todas
					</a>

				</header>

				<div class="posts">
					<?php foreach ($posts as $post): ?>
						<?php 
						$title = $post->post_title; 
						$exc = get_the_excerpt($post->ID);
						$img = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
						$link = get_the_Permalink($post->ID);
						// $date = get_the_date('', $post->ID);
						$date = human_time_diff( get_the_time('U', $post->ID), current_time('timestamp') ) . ' atrás';
						$author = get_userdata($post->post_author);
						$occupation = get_the_author_meta('occupation', $author->ID);
						$author_name = get_the_author_meta('first_name', $author->ID).' '.get_the_author_meta('last_name', $author->ID);
						$avatar = get_avatar_url($author->ID);

						$cats = get_the_terms($post->ID, 'category');

						?>
						<div class="post col-xs-12 col-sm-6 col-md-4">
							<a href="<?php echo $link; ?>" class="box">
								<div class="img-block" style="background-image: url('<?php echo $img; ?>');">
									<img src="<?php echo $img ?>" alt="<?php echo $title; ?>" class="hidden">
									<?php if($cats): ?>
									
									<!--botão bem estar na primeira foto da home-->
										<!-- <ul class="categories">
											<?php foreach ($cats as $c): ?>
												<?php 
												$name = $c->name;
												$color = get_term_meta($c->term_id, 'color', true);
												?>
												<li class="cat">
													<span style="background-color: <?php echo $color; ?>; box-shadow: 0 0 8px <?php echo $color; ?>;">
														<?php echo $name; ?>
													</span>
												</li>
											<?php endforeach; ?>
										</ul> -->
									<!--botão bem estar na primeira foto da home-->

									<?php endif; ?>
								</div>
								<div class="content">
									<h3 class="title"><?php echo $title; ?></h3>
									<p class="excerpt"><?php echo $exc; ?></p>
									<div class="post-meta">
										<div class="author">
											<div class="photo" style="background-image: url('<?php echo $avatar; ?>');">
												<img class="hidden" src="<?php echo $avatar; ?>">
											</div>
											<div class="infos">
												<strong class="name"><?php echo $author_name; ?></strong>
												<span class="occupation"><?php echo $occupation; ?></span>
											</div>
										</div>
										<span class="entry-date">
											<?php echo $date; ?>
										</span>
									</div>
								</div>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
				
				<!-- categorias coloridas-->								
				<div class="container">
					<div class="categories-title">Acompanhe as postagens por temas</div>
					
					<div class="categories-container">
						<?php 
						$args = array(
							'taxonomy' => 'category',
							'orderby' => 'name',
							'order ' => 'ASC',
							''
						);
						$cats = get_terms($args);
						if($cats):
						?>
							<ul class="categories">
								<?php foreach ($cats as $c): ?>
									<?php 
									$name = $c->name;
									$link = get_term_link($c->term_id);
									$color = get_term_meta($c->term_id, 'color', true);
									$darkencolor = adjustBrightness($color, -50);
		
									list($r, $g, $b) = sscanf($color, "#%02x%02x%02x");
						
									?>
									<li class="cat"> 
										<a href="<?php echo $link; ?>" style="background-color: rgba(<?php echo $r . ',' . $g . ',' . $b ?>, 0.2); color: <?php echo $darkencolor; ?>">
											<?php echo $name; ?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
				<!-- categorias coloridas-->

			</div>
		</div>
	</section>

<?php endif; ?>

