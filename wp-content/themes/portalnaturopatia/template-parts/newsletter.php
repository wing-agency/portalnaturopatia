<div class="newsletter" id="newsletter">
	<div class="container">
		<div class="row">
			<header class="header col-xs-12 col-sm-6">
				<h2 class="title">Assine nossa newsletter</h2>
				<p>e receba novidades e conteúdos diretamente em seu e-mail</p>
			</header>
			<div class="form-container col-xs-12 col-sm-6">
				<form class="form" action="" id="newsletter-form">
					<div class="form-group">
						<input class="form-control" type="email" placeholder="Seu melhor e-mail" name="newsletter-email" id="newsletter-email" required>
						<button type="submit" class="btn btn-secondary">Assinar</button>
					</div>
					
				</form>
				<span class="loader"></span>
                <div id="newsletter-content-alert"></div>
            </div>
		</div>
	</div>
</div>