<?php 
$args = array(
	'post_type' => 'post',
	'posts_per_page' => 3,
	'exclude' => get_the_id()
);
$posts = get_posts($args); 
if($posts):
?>	
<div class="related-posts">
	<div class="container">
		<header class="header">
			<h2 class="title">Leia também</h2>
		</header>

		<div class="row posts">
			<?php foreach ($posts as $post): ?>
				<?php 
				$title = $post->post_title; 
				$img = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
				$link = get_the_Permalink($post->ID);
				$date = human_time_diff( get_the_time('U', $post->ID), current_time('timestamp') ) . ' atrás';

				$author = get_userdata($post->post_author);
				$occupation = get_the_author_meta('occupation', $author->ID);
				$author_name = get_the_author_meta('first_name', $author->ID).' '.get_the_author_meta('last_name', $author->ID);
				$avatar = get_avatar_url($author->ID);

				$cats = get_the_terms($post->ID, 'category');
				?>
				<div class="post col-xs-12 col-sm-4">
					<a href="<?php echo $link; ?>" class="box">
						<div class="img-block" style="background-image: url('<?php echo $img; ?>');">
							<img src="<?php echo $img ?>" alt="<?php echo $title; ?>" class="hidden">
							<?php if($cats): ?>
								<ul class="categories">
									<?php foreach ($cats as $c): ?>
										<?php 
										$name = $c->name;
										$color = get_term_meta($c->term_id, 'color', true);
										?>
										<li class="cat">
											<span style="background-color: <?php echo $color; ?>; box-shadow: 0 0 8px <?php echo $color; ?>;">
												<?php echo $name; ?>
											</span>
										</li>
									<?php endforeach; ?>
								</ul>
							<?php endif; ?>
						</div>
						<div class="content">
							<h3 class="title"><?php echo $title; ?></h3>
							<p class="excerpt"><?php echo $exc; ?></p>
							<div class="post-meta">
								<div class="author">
									<div class="photo" style="background-image: url('<?php echo $avatar; ?>');">
										<img class="hidden" src="<?php echo $avatar; ?>">
									</div>
									<div class="infos">
										<strong class="name"><?php echo $author_name; ?></strong>
										<span class="occupation"><?php echo $occupation; ?></span>
									</div>
								</div>
								<span class="entry-date">
									<?php echo $date; ?>
								</span>
							</div>
						</div>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?php endif; ?>