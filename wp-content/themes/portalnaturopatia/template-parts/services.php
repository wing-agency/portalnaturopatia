<?php 
$args = array(
	'post_type' => 'services',
	'numberposts' => -1
);
$services = get_posts($args);
?>
<section class="home-services">
	<div class="container">
		<div class="row">
			<?php foreach ($services as $s): ?>
				<?php 
				$title = $s->title;
				$icon = wp_get_attachment_url(get_post_meta($s->ID, 'icon', true));
				$desc = get_post_meta($s->ID, 'description', true);
				$button_text = get_post_meta($s->ID, 'button_text', true);
				$button_link = get_post_meta($s->ID, 'button_link', true);
				$color = get_post_meta($s->ID, 'color', true);

				if($color == 'blue')
					$class = 'primary';
				else if($color == 'green')
					$class = 'secondary';
				?>
				<div class="service <?php echo $class; ?> col-xs-12 col-sm-6 col-md-4">
					<div class="box">
						<div class="icon" style="background-image: url('<?php echo $icon; ?>');">
							<img class="hidden" src="<?php echo $icon; ?>">
						</div>
						<h2 class="title"><?php echo $title; ?></h2>
						<?php if($desc): ?>
							<p class="description"><?php echo $desc; ?></p>
						<?php endif; ?>
						<a class="btn btn-<?php echo $class; ?>" href="<?php echo $button_link; ?>"><?php echo $button_text; ?></a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>