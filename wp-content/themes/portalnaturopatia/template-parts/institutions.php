<?php 
$args = array(
	'post_type' => 'institution',
	'numberposts' => 6,
	'orderby' => 'post_title',
	'order' => 'ASC'
);
$institutions = get_posts($args);
?>
<section class="home-institutions">
	<div class="container">
		<div class="row">
			<header class="header col-xs-12 ">
				<h2 class="title">Instituições associadas</h2>

				<a class="" href="<?php echo esc_url( home_url('/insituicoes') ); ?>">
					Ver todas
				</a>

			</header>
		</div>
		<div class="row institutions">
			<?php foreach ($institutions as $inst): ?>
				<?php 
				$title = $inst->title;
				$logo = get_post_meta($inst->ID, 'logo', true)['guid'];
				$link = get_the_permalink($inst->ID);
				?>
				<div class="institution col-xs-6 col-sm-3 col-md-2">
					<a class="box" href="<?php echo $link; ?>">
						<div class="logo" style="background-image: url('<?php echo $logo; ?>');">
							<img class="hidden" src="<?php echo $logo; ?>">
						</div>
						<h3 class="hidden"><?php echo $title; ?></h3>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>