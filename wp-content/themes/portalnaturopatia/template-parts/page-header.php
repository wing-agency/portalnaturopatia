<?php 
if(is_home()){
	$page = 'blog-header';
}else if(is_page('login')){
	$page = 'login-header';
}else if(is_page('criar-conta')){
	$page = 'register-header';
}
?>

<section class="page-header <?php  echo $page; ?>">
	<div class="container">
		<div class="row">
			<header class="entry-header ">

				<h1 class="entry-title">
					<?php if(is_home()): ?>
						Nosso blog
					<?php else: ?>
						<?php the_Title(); ?>
					<?php endif; ?>
				</h1>

				<?php if(is_home() && is_paged()): ?>
					<?php  $paged = get_query_var( 'paged', 1 );  ?>
					<p class="page-number">Página <?php echo (int) $paged; ?></p>
				<?php endif; ?>
				
				<?php if(is_home()): ?>
					<div class="categories-container">
						<?php 
						$args = array(
							'taxonomy' => 'category',
							'orderby' => 'name',
							'order ' => 'ASC',
							''
						);
						$cats = get_terms($args);
						if($cats):
						?>
							<ul class="categories">
								<?php foreach ($cats as $c): ?>
									<?php 
									$name = $c->name;
									$link = get_term_link($c->term_id);
									$color = get_term_meta($c->term_id, 'color', true);
									?>
									<li class="cat">
										<a href="<?php echo $link; ?>" style="background-color: <?php echo $color; ?>;">
											<?php echo $name; ?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>
					<?php get_search_form(); ?>
				<?php endif; ?>
			</header>
		</div>
	</div>
</section>