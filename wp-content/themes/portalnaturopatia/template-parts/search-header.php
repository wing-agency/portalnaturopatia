<section class="page-header search-header">
	<div class="container">
		<div class="row">
			<header class="entry-header col-xs-12">

				<h1 class="entry-title">
					<?php printf( __( '<span>Resultados da busca para:</span> %s', 'odin' ), get_search_query() ); ?>
				</h1>

				<?php if(is_search() && is_paged()): ?>
					<?php  $paged = get_query_var( 'paged', 1 );  ?>
					<p class="page-number">Página <?php echo (int) $paged; ?></p>
				<?php endif; ?>
				
				<?php get_search_form(); ?>
			</header>
		</div>
	</div>
</section>