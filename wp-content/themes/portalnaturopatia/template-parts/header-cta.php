<div class="header-cta col-sm-4 col-md-3">
	<?php if (is_user_logged_in()): ?>
		<a class="btn btn-link" href="<?php echo esc_url( home_url( '/minha-conta/?section=dashboard' ) ); ?>" >
			Minha conta
		</a>
	<?php else: ?>
		<a class="btn btn-link" href="<?php echo esc_url( home_url( '/login' ) ); ?>" >
			Entrar
		</a>
		<a class="btn btn-primary" href="<?php echo esc_url( home_url( '/criar-conta' ) ); ?>" >
			Cadastre-se
		</a>
	<?php endif; ?>

	
</div>