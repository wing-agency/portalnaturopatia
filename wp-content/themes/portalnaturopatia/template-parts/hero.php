<?php 
$hero = get_option('odin_hero'); 
$title = $hero['hero_title'];
?>
<section id="home-hero" class="home-hero" >
	<div class="container">
		<div class="row">
			<div class="content col-xs-12 col-md-6">
				<h2 class="title"><?php echo $title; ?></h2>
			</div>
			<div class="hero-actions col-xs-12 col-sm-6">
				<?php get_search_form(); ?>
				<!-- <div class="find-professionals">
					<p>Encontre um profissional de naturopatia</p>
					<a class="btn btn-primary" href="#">Encontrar profissionais</a>
				</div> -->
			</div>
			
		</div>
	</div>

</section><!-- #main -->