<?php $color = get_term_meta(get_queried_object_id(), 'color', true); ?>
<section class="page-header category-header" style="background-color: <?php echo $color; ?>">
	<div class="container">
		<div class="row">
			<header class="entry-header ">

				<div class="source">
					<a href="<?php echo esc_url( home_url( '/blog' ) ); ?>">Blog</a>
				</div>

				<h1 class="entry-title">
					<?php if(is_category()): ?>
						<?php //echo get_queried_object()->name; ?>
						<?php the_archive_title(); ?>
					<?php endif; ?>
				</h1>

				<?php if(is_paged()): ?>
					<?php  $paged = get_query_var( 'paged', 1 );  ?>
					<p class="page-number">Página <?php echo (int) $paged; ?></p>
				<?php endif; ?>
				
			</header>
		</div>
	</div>
</section>