<?php 
$authors = get_users( array(
    'orderby'       => 'name',
    'order'         => 'ASC',
    'number'        => -1
) );
?>

<section class="home-professionals">
	<div class="container">
		<div class="row">
			<header class="header col-xs-12 ">
				<h2 class="title">Profissionais em destaque</h2>

				<a class="" href="<?php echo esc_url( home_url('/profissionais') ); ?>">
					Ver todos
				</a>

			</header>
		</div>
		<div class="row professionals">
			<?php foreach ($authors as $author): ?>
				<?php 
				$occupation = get_the_author_meta('occupation', $author->ID);
				$author_name = get_the_author_meta('first_name', $author->ID).' '.get_the_author_meta('last_name', $author->ID);
				$avatar = get_avatar_url($author->ID);
				$link = get_author_posts_url($author->ID);
				?>
				<div class="professional col-xs-6 col-sm-3 col-md-2">
					<a class="box" href="<?php echo $link; ?>">
						<div class="avatar" style="background-image: url('<?php echo $avatar; ?>');">
							<img class="hidden" src="<?php echo $avatar; ?>">
						</div>
						
						<h3 class="name"><?php echo $author_name; ?></h3>
						<span class="occupation"><?php echo $occupation; ?></span>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="row">
			<div class="find-professionals col-xs-12">
				<h3 class="title">Encontre o profissional ideal para você</h3>
				<p class="description">Escolha o tipo de profissional</p>
				<ul class="occupations">
					<li class="occupation">
						<a href="#">Ed. Física</a>
					</li>
					<li class="occupation">
						<a href="#">Nutrição</a>
					</li>
					<li class="occupation">
						<a href="#">Medicina</a>
					</li>
					<li class="occupation">
						<a href="#">Quiropraxia</a>
					</li>
					<li class="occupation">
						<a href="#">Fisioterapia</a>
					</li>
				</ul>
				<div class="submit">
					<a class="btn btn-primary" href="#">Buscar</a>
				</div>
			</div>
		</div>
	</div>
</section>