<section class="page-header archive-header" >
	<div class="container">
		<div class="row">
			<header class="entry-header ">

				<h1 class="entry-title">
					<?php the_archive_title(); ?>
				</h1>
				
			</header>
		</div>
	</div>
</section>