<?php 
$title = get_the_title();
$date = get_the_date('', get_the_ID());
?>

<div class="post-header">

	<div class="container">
		<div class="row">
			<header class="entry-header col-xs-12 col-md-8 col-md-offset-2">

				<div class="source">
					<a href="<?php echo esc_url( home_url( '/blog' ) ); ?>">Blog</a>
				</div>

				<h1 class="entry-title"><?php echo $title; ?></h1>

				<div class="date"><?php echo $date; ?></div>

				<div class="share-container">
					<span>Compartilhar</span>
					<?php echo do_shortcode('[addtoany]'); ?>
				</div>
			</header>
		</div>
	</div>
</div>