<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part('template-parts/page-header'); ?>

		<main id="content" class="container page-professionals-content <?php //echo odin_classes_page_full(); ?>" tabindex="-1" role="main">

			<?php
				// Start the Loop.
				// while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				// endwhile;
			?>

			<div class="row">
				<div class="find-professionals col-xs-12">
					<h3 class="title">Encontre o profissional ideal para você</h3>
					<p class="description">Escolha o tipo de profissional</p>
					<ul class="occupations">
						<li class="occupation">
							<a href="#">Ed. Física</a>
						</li>
						<li class="occupation">
							<a href="#">Nutrição</a>
						</li>
						<li class="occupation">
							<a href="#">Medicina</a>
						</li>
						<li class="occupation">
							<a href="#">Quiropraxia</a>
						</li>
						<li class="occupation">
							<a href="#">Fisioterapia</a>
						</li>
					</ul>
					<div class="submit">
						<a id="find-professionals-submit" class="btn btn-primary" href="#">Buscar</a>
					</div>
				</div>
			</div>

			<?php 
			$authors = get_users( array(
			    'orderby'       => 'name',
			    'order'         => 'ASC',
			    'number'        => -1
			) );
			?>
			<div class="row professionals hidden">
				<?php foreach ($authors as $author): ?>
					<?php 
					$occupation = get_the_author_meta('occupation', $author->ID);
					$author_name = get_the_author_meta('first_name', $author->ID).' '.get_the_author_meta('last_name', $author->ID);
					$avatar = get_avatar_url($author->ID);
					$link = get_author_posts_url($author->ID);
					?>
					<div class="professional col-xs-6 col-sm-3 col-md-2">
						<a class="box" href="<?php echo $link; ?>">
							<div class="avatar" style="background-image: url('<?php echo $avatar; ?>');">
								<img class="hidden" src="<?php echo $avatar; ?>">
							</div>
							
							<h3 class="name"><?php echo $author_name; ?></h3>
							<span class="occupation"><?php echo $occupation; ?></span>
						</a>
					</div>
				<?php endforeach; ?>
			</div>

		</main><!-- #main -->

		
<?php endwhile; ?>

<?php
get_footer();
