<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/search-header'); ?>

<div class="container">
	<div class="row">
		<?php if ( have_posts() ) : ?>
			<main id="content" class="posts <?php// echo odin_classes_page_sidebar(); ?>" tabindex="-1" role="main">

				

				<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();

						/*
						 * Include the post format-specific template for the content. If you want to
						 * use this in a child theme, then include a file called called content-___.php
						 * (where ___ is the post format) and that will be used instead.
						 */
						get_template_part( 'content', 'post' );

					endwhile;

					// Post navigation.
					odin_paging_nav();
				?>
		<?php
		else : ?>
			<main id="content" class="no-results" tabindex="-1" role="main">
		
			<?php
			// If no content, include the "No posts found" template.
			get_template_part( 'content', 'none' );

		endif;
		?>

			</main><!-- #content -->
	</div>
</div>

<?php
// get_sidebar();
get_footer();
