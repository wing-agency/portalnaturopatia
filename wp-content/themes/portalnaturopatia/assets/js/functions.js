jQuery(document).ready(function($) {
	$.fancybox.defaults.hash = false;

	$('.navbar-toggle').click(function(event) {
		event.preventDefault();
		$(this).toggleClass('collapsed');
		$('.navbar-main-navigation').toggleClass('in');
		$('.header-cta').toggleClass('in');
		$('#wpadminbar').toggleClass('navbar-in');
		if($('.navbar-main-navigation').hasClass('in')){
			// $(window).scroll(function () { $(window).scrollTop(0); });
		}else{
			// $(window).off('scroll');
		}
	});

	$('.gallery-item').find('div').find('a').addClass('fancybox-media');

	$('.gallery').each(function(index, el) {
		//console.log($(this).attr('id'));
		$(this).find('.fancybox-media')
		.attr('data-fancybox', $(this).attr('id'))
		.fancybox({
			openEffect : 'elastic',
			closeEffect : 'elastic',
			fullScreen : false,
			thumbs     : false,
			infobar : true,
			hash: null
		});
		
	});

	$('.blocks-gallery-item').find('figure').find('a').addClass('fancybox-media');

	$('.wp-block-gallery').each(function(index, el) {
		//console.log($(this).attr('id'));
		$(this).find('.fancybox-media')
		.attr('data-fancybox', 'gallery-'+index)
		.fancybox({
			openEffect : 'elastic',
			closeEffect : 'elastic',
			fullScreen : false,
			thumbs     : false,
			infobar : true,
			hash: null
		});
		
	});

	$('.find-professionals li a').click(function(event) {
		event.preventDefault();
		$('.find-professionals li').removeClass('active');
		$(this).parent().addClass('active');
	});

	$('#find-professionals-submit').click(function(event) {
		$('.professionals').removeClass('hidden');
	});

});

