<?php
/**
 * The template for displaying Author archive pages.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

<?php 
	

	$occupation = get_the_author_meta('occupation', get_queried_object_id());
	$author_name = get_the_author_meta('first_name', get_queried_object_id()).' '.get_the_author_meta('last_name', get_queried_object_id());
	$avatar = get_avatar_url(get_queried_object_id());

	$phone = get_the_author_meta('phone', get_queried_object_id());
	$email = get_the_author_meta('email', get_queried_object_id());
	$specialties = get_the_author_meta('especialidades', get_queried_object_id());

	$number = preg_replace("/[^0-9]/", "", $phone);
	$whats_link = 'https://wa.me/55'.$number;
?>
<section class="author-header" >
	<div class="container">
		<div class="row">
			<header class="entry-header col-xs-12">
				<div class="avatar" style="background-image: url('<?php echo $avatar; ?>');">
					<img class="hidden" src="<?php echo $avatar; ?>">
				</div>
				<div class="infos">
					<h1 class="entry-title">
						<?php echo $author_name; ?>
					</h1>
					<p class="occupation">
						<?php echo $occupation; ?>
					</p>
					<p class="institution">
						Instituição: <strong><?php echo 'UNIPAR'; ?></strong>
					</p>
					<p class="email">
						E-mail: <strong><?php echo $email; ?></strong>
					</p>
					<p class="phone">
						Telefone: <strong><?php echo $phone; ?></strong>
					</p>
					<p class="WhatsApp">
						<a class="btn btn-secondary" href="<?php echo $whats_link; ?>">Marcar consulta</a>
					</p>
				</div>
			</header>
			<div class="specialties col-xs-12 col-md-8">
				<h2 class="title">Especialidades</h2>
				<?php echo apply_filters('the_content', $specialties); ?>
			</div>
		</div>
	</div>
</section>


<?php if ( have_posts() ) : the_post();?>
<div class="container">
	<div class="row">
		<header class="author-posts-header col-xs-12">
			<h2 class="title">Publicações de <?php echo $author_name; ?></h2>
		</header>
		<main id="content" class="posts <?php// echo odin_classes_page_sidebar(); ?>" tabindex="-1" role="main">

			
				

				<?php
						/*
						 * Since we called the_post() above, we need to rewind
						 * the loop back to the beginning that way we can run
						 * the loop properly, in full.
						 */
						rewind_posts();

						// Start the Loop.
						while ( have_posts() ) : the_post();

							/*
							 * Include the post format-specific template for the content. If you want to
							 * use this in a child theme, then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							get_template_part( 'content', 'post' );

						endwhile;

						// Page navigation.
						odin_paging_nav();

					// else :
						// If no content, include the "No posts found" template.
						// get_template_part( 'content', 'none' );

				
			?>

		</main><!-- #content -->
	</div>
</div>
<?php endif; ?>

<?php
// get_sidebar();
get_footer();
