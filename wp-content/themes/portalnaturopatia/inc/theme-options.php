<?php 

function odin_theme_settings() {

	$odin_theme_options = new Odin_Theme_Options( 
	    'theme-options', // Slug/ID da página (Obrigatório)
	    __( 'Opções do site', 'odin' ), // Titulo da página (Obrigatório)
	    'manage_options' // Nível de permissão (Opcional) [padrão é manage_options]
	);

	$odin_theme_options->set_tabs(
	    array(
	        // array(
	        //     'id' => 'odin_general', // ID da aba e nome da entrada no banco de dados.
	        //     'title' => __( 'Configurações Gerais', 'odin' ), // Título da aba.
	        // ),
            array(
                'id' => 'odin_social', // ID da aba e nome da entrada no banco de dados.
                'title' => __( 'Redes Sociais', 'odin' ), // Título da aba.
            ),
	        array(
	            'id' => 'odin_hero', // ID da aba e nome da entrada no banco de dados.
	            'title' => __( 'Capa do site', 'odin' ), // Título da aba.
	        )
	    )
	);

	$odin_theme_options->set_fields(
        array(
            // 'odin_general_fields_section' => array( // Slug/ID of the section (Required)
            //     'tab'   => 'odin_general', // Tab ID/Slug (Required)
            //     'title' => __( 'Informações gerais', 'odin' ), // Section title (Required)
            //     'fields' => array( // Section fields (Required)
            //         array(
            //             'id'          => 'email', // Required
            //             'label'       => __( 'E-mail', 'odin' ), // Required
            //             'type'        => 'input', // Required
            //             // 'default'  => 'Default text', // Optional
            //             'description' => __( 'Informe o email para contato', 'odin' ), // Optional
            //             'attributes'  => array( // Optional (html input elements)
            //                 'type' => 'email'
            //                 // 'required' => 'required'
            //             )
            //         ),
            //         array(
            //             'id'          => 'phone', // Required
            //             'label'       => __( 'Telefone', 'odin' ), // Required
            //             'type'        => 'input', // Required
            //             // 'default'  => 'Default text', // Optional
            //             'description' => __( 'Informe o número do telefone para contato', 'odin' ), // Optional
            //             'attributes'  => array( // Optional (html input elements)
            //                 'type' => 'text'
            //             )
            //         ),
            //         array(
            //             'id'          => 'whatsapp', // Required
            //             'label'       => __( 'WhatsApp', 'odin' ), // Required
            //             'type'        => 'input', // Required
            //             // 'default'  => 'Default text', // Optional
            //             'description' => __( 'Informe o número do WhatsApp', 'odin' ), // Optional
            //             'attributes'  => array( // Optional (html input elements)
            //                 'type' => 'text'
            //             )
            //         ),
            //         array(
            //             'id'          => 'address', // Required
            //             'label'       => __( 'Endereço', 'odin' ), // Required
            //             'type'        => 'textarea', // Required
            //             // 'default'  => 'Default text', // Optional
            //             'description' => __( 'Informe o endereço da empresa', 'odin' ), // Optional
            //             'attributes'  => array( // Optional (html input elements)
            //                 // 'type' => 'text'
            //             )
            //         )
            //     )
            // ),
            'odin_social_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'odin_social', // Tab ID/Slug (Required)
                'title' => __( 'Redes sociais', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)

                    array(
                        'id'          => 'facebook', // Required
                        'label'       => __( 'Facebook', 'odin' ), // Required
                        'type'        => 'input', // Required
                        // 'default'  => 'Default text', // Optional
                        'description' => __( 'Informe o link da página do Facebook', 'odin' ), // Optional
                        'attributes'  => array( // Optional (html input elements)
                            'type' => 'text'
                        )
                    ),
                    array(
                        'id'          => 'instagram', // Required
                        'label'       => __( 'Instagram', 'odin' ), // Required
                        'type'        => 'input', // Required
                        // 'default'  => 'Default text', // Optional
                        'description' => __( 'Informe o link do perfil do Instagram', 'odin' ), // Optional
                        'attributes'  => array( // Optional (html input elements)
                            'type' => 'text'
                        )
                    ),
                    array(
                        'id'          => 'linkedin', // Required
                        'label'       => __( 'Linkedin', 'odin' ), // Required
                        'type'        => 'input', // Required
                        // 'default'  => 'Default text', // Optional
                        'description' => __( 'Informe o link do perfil do Linkedin', 'odin' ), // Optional
                        'attributes'  => array( // Optional (html input elements)
                            'type' => 'text'
                        )
                    )
                )
            ),
            'odin_hero_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'odin_hero', // Tab ID/Slug (Required)
                'title' => __( 'Capa do site', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)

                	array(
                        'id'          => 'hero_title', // Required
                        'label'       => __( 'Título', 'odin' ), // Required
                        'type'        => 'textarea', // Required
                        // 'default'  => 'Default text', // Optional
                        'description' => __( 'Informe o título principal', 'odin' ), // Optional
                        'attributes'  => array( // Optional (html input elements)
                            // 'type' => 'text'
                        )
                    )
                )
            )
        )
    );
}

add_action( 'init', 'odin_theme_settings', 1 );