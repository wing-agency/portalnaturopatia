<?php

/**
* Filtrar título da página archive
*/
function wp_archive_title_filter() {
    if (is_category()) {
        $title = sprintf( __( 'Category: %s' ), single_cat_title( '', false ) );
    }else{
        $title = post_type_archive_title( '', false );
    }

    
    return $title;
}
add_filter('get_the_archive_title','wp_archive_title_filter', 10);

/**
* Configurar tamanho do excerpt
*/
function custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/**
* setar pesquisa somente para posts do blog
*/
if (!is_admin()) {
    function wp_search_filter($query) {
        if ($query->is_search) {
            $query->set('post_type', array('post'));
        }
        return $query;
    }
    add_filter('pre_get_posts','wp_search_filter');
}

/**
* Adicionar um campo no registro de usuários para selecionar a instituição
*/
function add_user_institution_form_field( $form_id, $post_id, $form_settings ) {

	$institution_id = '';
	$user_id = get_current_user_id();

	if($user_id){
		$institution_id = get_user_meta($user_id, 'wpuf_institution', true);
	}

	$args = array(
		'post_type' => 'institution',
		'numberposts' => -1,
		'orderby' => 'post_title',
		'order' => 'ASC'
	);
	$institutions = get_posts($args);
    ?>
    <li class="wpuf-el institution field-size-large" data-label="Instituição">
        <div class="wpuf-label">
            <label for="institution">Instituição</label>
        </div>
        <div class="wpuf-fields">
            <!-- <input class="textfield wpuf_institution" id="institution" type="text" data-required="yes" data-type="text" name="institution" placeholder="" value="<?php echo $institution; ?>" size="40"> -->
            <select class="wpuf_dropdown" id="wpuf_institution" name="wpuf_institution" data-required="yes" data-type="select">

            	<option value="-1">- Selecione a instituição - </option>

	            <?php foreach ($institutions as $i):?>
	            	<?php $sigla = get_post_meta($i->ID, 'sigla', true); ?>
	            	<option data-user="<?php echo $user_id; ?>" value="<?php echo $i->ID; ?>" <?php echo $institution_id == $i->ID ? 'selected' : ''; ?>>
	            		<?php echo $sigla; ?> - <?php echo $i->post_title; ?>
	            	</option >
	            <?php endforeach; ?>

            </select>

            <span class="wpuf-wordlimit-message wpuf-help"></span>
        </div>
    </li>
    <?php
}
add_action('user_institution', 'add_user_institution_form_field', 10, 3 );

/**
* Salvar a informação da instituição quando criar ou atualizar os dados do usuário
*/
function update_user_institution_info( $post_id ) {
    // if ( isset( $_POST['wpuf_institution'] ) ) {
    //     update_post_meta( $post_id, 'wpuf_institution', $_POST['wpuf_institution'] );
    // }
    echo 'teste';
}
 
// add_action( 'wpuf_update_profile', 'update_user_institution_info' );
// add_action( 'wpuf_after_register', 'update_user_institution_info' );

// add_action('personal_options_update', 'update_extra_profile_fields');
 
 function update_extra_profile_fields($user_id) {
 	echo 'teste';
    if ( current_user_can('edit_user',$user_id) )
         update_user_meta($user_id, 'wpuf_institution', $_POST['wpuf_institution']);
 }