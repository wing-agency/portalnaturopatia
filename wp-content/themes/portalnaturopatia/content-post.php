<?php
/**
 * The default template for displaying content.
 *
 * Used for both single and index/archive/author/catagory/search/tag.
 *
 * @package Odin
 * @since 2.2.0
 */
?>

<?php 
global $post;
$title = get_the_title(); 
$img = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
$link = get_the_Permalink(get_the_ID());
// $date = get_the_date('', get_the_ID());
$date = human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' atrás';

$author = get_userdata($post->post_author);
$occupation = get_the_author_meta('occupation', $author->ID);
$author_name = get_the_author_meta('first_name', $author->ID).' '.get_the_author_meta('last_name', $author->ID);
$avatar = get_avatar_url($author->ID);

$cats = get_the_terms(get_the_ID(), 'category');

if ( !is_single() ) :
?>
	<article class="post col-xs-12 col-sm-4">
		<a href="<?php echo $link; ?>" class="box">
			<div class="img-block" style="background-image: url('<?php echo $img; ?>');">
				<img src="<?php echo $img ?>" alt="<?php echo $title; ?>" class="hidden">
				<?php if($cats): ?>
					<ul class="categories">
						<?php foreach ($cats as $c): ?>
							<?php 
							$name = $c->name;
							$color = get_term_meta($c->term_id, 'color', true);
							?>
							<li class="cat">
								<span style="background-color: <?php echo $color; ?>; box-shadow: 0 0 8px <?php echo $color; ?>;">
									<?php echo $name; ?>
								</span>
							</li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</div>
			<div class="content">
				<h3 class="title"><?php echo $title; ?></h3>
				<p class="excerpt"><?php echo $exc; ?></p>
				<div class="post-meta">
					<div class="author">
						<div class="photo" style="background-image: url('<?php echo $avatar; ?>');">
							<img class="hidden" src="<?php echo $avatar; ?>">
						</div>
						<div class="infos">
							<strong class="name"><?php echo $author_name; ?></strong>
							<span class="occupation"><?php echo $occupation; ?></span>
						</div>
					</div>
					<span class="entry-date">
						<?php echo $date; ?>
					</span>
				</div>
			</div>
		</a>
	</article>
<?php 
else : ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php
			the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'odin' ) );
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'odin' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->
	<div class="entry-meta">
		<div class="share-container">
			<span>Compartilhar</span>
			<?php echo do_shortcode('[addtoany]'); ?>
		</div>
	</div>
</article><!-- #post-## -->
<?php endif; ?>