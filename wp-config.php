<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'portal_naturopatia' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'NC!D)?IL3WQ*]>IzUb=[7OvKwk24TbxdlZ_#^nW<@o=LFY$Q6&|@$~js`ck]Pbd5' );
define( 'SECURE_AUTH_KEY',  'hG<;sn` e0x5Hs|2=YXxh]o<=}6;#1R0g M(3we-L!__MD-9x*1CBxz:JJn^CP]+' );
define( 'LOGGED_IN_KEY',    'LN>^,7l{bFN[Qlw6?[/U]#yEq#r/>RE@` ;Xb2q2v9#kteT7=M10,J@{&=kJN|g=' );
define( 'NONCE_KEY',        'm11/dX4`g&wbxPaZBl}Z:0Qa#(W8N9mk^Gm[H!+P[O*j?*<pW/%^KY;t4 B!.oXp' );
define( 'AUTH_SALT',        'jN[_7hk55.Or w<|FbLpVPv@F+5GP.oBl39A2Yj?Mn^?.!8y|S@?+,;d uA}zD.y' );
define( 'SECURE_AUTH_SALT', 'Y0.pPG|2R)rO+PR[&e&H*hNj/KRTh)W[{^..S_dX*+nHCBPlYoa}X6[QZF:+O`v9' );
define( 'LOGGED_IN_SALT',   '3o-T`v?n4GRqgb/`j<P6o^zh(/Do69c-B$<Cv>7bj)vo&Gq7v,bm(#qgc;us1?rH' );
define( 'NONCE_SALT',       '&LlDdQY}?K<1vv]9hR|m=RHoS)n~gH0v?kc,,dWq:5AAt$c!{eR/gx}>cZcLBZ.s' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
